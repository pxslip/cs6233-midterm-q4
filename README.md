#A basic workflow application for CSCI 6233 at the George Washington University#

##Usage##
- Create a new workflow definition file
  - Workflow definitions are stored in the public/workflows folder
  - Workflow definitions are stored in JSON format with the following schema (for an example look at public/workflows/demo.json and visit http://localhost:3000/demo):
    - The top level is an object with two properties:
      - title: a string name for this workflow
      - steps: an array of objects (steps) defining the nodes and links between nodes
        - A step has three properties:
          - name: the name of the step (required)
          - next: the index of the next step or false if the last step (required)
          - alt: the index of the parallel alternate step (optional)
          - Note: To prevent strange layouts it is recommended that the value of next be less than alt
  - Once finished save the workflow, the name should contain no spaces or special characters (e.g. workflow-1, or my-awesome-workflow)
  - To load the workflow, start the application (see Run below for details)
  - Visit http://localhost:3000/workflow-name, where workflow-name is the name of the workflow you just created

##Run##
- To run this application locally you must have [nodejs](http://nodejs.org) installed
- Once nodejs is installed cd to the root of the project
- Run `npm install` to install the dependencies
- Ensure that bin/www has execute privileges (chmod +x bin/www)
- Run bin/www to start the application on port 3000

##Running tests##
- You will need to have installed the grunt-cli by running `npm install -g grunt-cli`
- You will also need bower installed through `npm install -g bower`
- Once grunt and bower are installed cd into the project
- Run `npm install && bower install` to install dependencies
- Finally run `node bin/www 2>/dev/null 1>&2 &` followed by `grunt test` to run the test suites (w/o extra output from the server)
