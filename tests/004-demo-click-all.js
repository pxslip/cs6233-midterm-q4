var workflow = require(require('fs').pathJoin(require('system').env.PWD, 'public', 'workflows', 'demo.json'));
casper.test.begin('Ensure that all of the nodes can be clicked in order', workflow.steps.length, function suite(test) {
  casper.start('http://localhost:3000/demo', function() {
    workflow.steps.forEach(function(value, index) {
      if(index === 0) {
        //start by just clicking the first node
        casper.click('[model-id="step_0"]');
      }
      if(value.next) {
        casper.click('[model-id="step_'+value.next+'"]');
      }
      if(value.alt) {
        casper.click('[model-id="step_'+value.alt+'"]');
      }
      test.assertExists('[model-id="step_'+index+'"].clicked');
    });
  });
  casper.run(function() {
    test.done();
  });
});
