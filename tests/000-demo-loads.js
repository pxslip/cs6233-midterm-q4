var workflow = require(require('fs').pathJoin(require('system').env.PWD, 'public', 'workflows', 'demo.json'));
casper.test.begin('The demo configuration loads and all of the required elements are available', function suite(test) {
  casper.start('http://localhost:3000/demo', function() {
    //make sure the title matches what is expected
    test.assertTitle(workflow.title, 'Title is the expected value, matches workflow title from the JSON - ' + workflow.title);
    for(var i = 0; i < workflow.steps.length; i++) {
      test.assertExists('[model-id="step_'+i+'"]', 'The node step_'+i+' exists');
      if(i !== 0) {
        //check that we at least one entry link into every node, except the first node
        test.assertExists('[model-id$="to_'+i+'"]', 'The node step_'+i+' has at least one entry link');
      }
      if(i !== 20) {
        //check that we have at least one exit link for every node, except the last node
        test.assertExists('[model-id^="from_'+i+'"]', 'The node step_'+i+' has at least one exit link');
      }
    }
  });
  casper.run(function() {
    test.done();
  });
});
