casper.test.begin('Verify that the click status of the first step is retained after reload', 4, function suite(test) {
  casper.start('http://localhost:3000/demo', function() {
    //verify that the first node exists and is unclicked
    test.assertExists('[model-id="step_0"]', 'The first step with model-id="step_0" exists');
    //verify that the first node is unclicked
    test.assertDoesntExist('[model-id="step_0"].clicked', 'The first step does not have the .clicked class');
    //click on the first node
    casper.click('[model-id="step_0"]');
    //verify that the click appeared as a class change
    test.assertExists('[model-id="step_0"].clicked', 'The first step does have the .clicked class');
    casper.reload(function() {
      //test that the step_0 node is still clicked
      test.assertExists('[model-id="step_0"].clicked', 'The first step retains the .clicked class after reload');
    });
  });
  casper.run(function() {
    test.done();
  });
});
