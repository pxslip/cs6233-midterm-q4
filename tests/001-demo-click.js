casper.test.begin('The demo configuration loads and we can click the nodes in proper order', 3, function suite(test) {
  casper.start('http://localhost:3000/demo', function() {
    //verify that the first node exists and is unclicked
    test.assertExists('[model-id="step_0"]', 'The first step with model-id="step_0" exists');
    //verify that the first node is unclicked
    test.assertDoesntExist('[model-id="step_0"].clicked', 'The first step does not have the .clicked class');
    //click on the first node
    casper.click('[model-id="step_0"]');
    //verify that the click appeared as a class change
    test.assertExists('[model-id="step_0"].clicked', 'The first step does have the .clicked class');
  });
  casper.run(function() {
    test.done();
  });
});
