casper.test.begin('Attempt to click the second step, before clicking the first, ensuring that the program prevents this', function suite(test) {
  //first bind to the remote.alert event so we can verify that the alert appeared and contains the correct text
  casper.on('remote.alert', function(message) {
    this.test.assertEquals(message, 'please complete the previous step before this one', 'Test to ensure that steps must be completed in order');
  });
  casper.start('http://localhost:3000/demo', function() {
    //first
    test.assertExists('[model-id="step_0"]', 'The first step with model-id="step_0" exists');
    test.assertExists('[model-id="step_1"]', 'The second step with model-id="step_1" exists');
    //attempt to click step_1
    casper.click('[model-id="step_1"]');
  });
  casper.run(function() {
    test.done();
  });
});
