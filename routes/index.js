var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');

/* GET home page. */

router.get('/', function(req, res) {
  res.render('index', {
    title: 'q4'
  });
});

router.get('/:config', function(req, res) {
  var config_name = req.params.config;
  fs.readFile(path.join(__dirname, '..', 'public', 'workflows', config_name + '.json'), 'utf8', function(err, data) {
    if (err) {
      throw err;
    }
    var json = JSON.parse(data);
    res.render('index', {
      title: json.title,
      workflow_id: config_name,
      workflow: data,
      completion: (req.session[config_name]) ? JSON.stringify(req.session[config_name]) : '{steps_completed: []}',
      scripts: ['/js/index.js']
    });
  });
});

router.post('/record-step', function(req, res) {
  //push the step id into the session
  if (!req.session[req.body.title]) {
    req.session[req.body.title] = {};
    req.session[req.body.title].steps_completed = [];
  }
  req.session[req.body.title].steps_completed.push(parseInt(req.body.step, 10));
  res.send('Step Stored');
});

router.get('/destroy-session', function(req, res) {
  req.session.destroy(function(err) {
    res.send('Destroyed');
  });
});

module.exports = router;
