//execute all of the page side js here
var cs6233mtq4 = {
  workflow: null,
  graph: null,
  paper: null,
  nodes: [],
  _consts: {
    PARENT: 'parent',
    MODEL_ID: 'model-id'
  },
  execute: function(workflow, title, completion) {
    var self = this;
    //set the graph and paper needed to build the workflow
    self.graph = new joint.dia.Graph();
    self.paper = new joint.dia.Paper({
      el: $('#workflow'),
      width: 800,
      height: 2000,
      model: self.graph,
      gridsize: 1,
      perpendicularLinks: true,
      interactive: false
    });
    //iterate through the workflow once, making the step rects
    var yOffset = 0;
    workflow.steps.forEach(function(value, index, array) {
      var attrs,
          isComplete = completion.steps_completed.indexOf(index) !== -1;
      if(!isComplete) {
        attrs = {
          text: {
            text: value.name
          }
        };
      } else {
        attrs = {
          rect: {
            fill: 'black'
          },
          text: {
            text: value.name,
            fill: 'white'
          }
        };
      }
      var rect = new joint.shapes.basic.Rect({
        position: {
          x: 400,
          y: 10 + yOffset
        },
        size: {
          width: 100,
          height: 50
        },
        attrs: attrs,
        id: 'step_' + index
      });

      self.graph.addCell(rect);
      self.nodes.push(rect);
      if(isComplete) {
        rect.prop('clicked', true);
        V(document.querySelector('[model-id="'+rect.id+'"]')).addClass('clicked');
      }
      yOffset += 100;
    });
    //now make the links between elements in the graph
    workflow.steps.forEach(function(value, index, array) {
      if (value.next !== false) {
        var link = new joint.dia.Link({
          source: self.nodes[index],
          target: self.nodes[value.next],
          attrs: {
            '.marker-target': {
              fill: 'black',
              d: 'M 10 0 L 0 5 L 10 10 z'
            }
          },
          id: 'from_'+index+'_to_'+value.next
        });
        if(!self.nodes[value.next].prop(self._consts.PARENT)) {
          self.nodes[value.next].prop(self._consts.PARENT, index);
        } else {
          self.nodes[value.next].prop(self._consts.PARENT, self.nodes[value.next].prop(self._consts.PARENT) + ',' + index);
        }
        self.graph.addCell(link);
        if (value.alt) {
          //move the node that is being linked to the right of the parent
          var parentNode = self.nodes[index],
            altNode = self.nodes[value.alt],
            pos = parentNode.position();
          altNode.position(pos.x + 150, pos.y);
          if(!altNode.prop(self._consts.PARENT)) {
            altNode.prop(self._consts.PARENT, index);
          } else {
            altNode.prop(self._consts.PARENT, altNode.prop(self._consts.PARENT) + ',' + index);
          }
          self.graph.addCell(new joint.dia.Link({
            source: self.nodes[index],
            target: self.nodes[value.alt],
            attrs: {
              '.marker-target': {
                fill: 'black',
                d: 'M 10 0 L 0 5 L 10 10 z'
              }
            },
            id: 'from_'+index+'_to_'+value.alt
          }));
          //shift the rest of the nodes up to compensate
          for(var i = value.alt+1; i < self.nodes.length; i++) {
            self.nodes[i].translate(0, -100);
          }
        }
      }
    });
    $('.element.basic.Rect').on('click', function(event) {
      //this is where we handle the click event on a given node
      var index = $(this).attr(self._consts.MODEL_ID).replace('step_', ''),
          node = self.nodes[index],
          parents = node.prop(self._consts.PARENT),
          parentClicked = false;
      if(parents !== undefined) {
        parents.toString().split(',').forEach(function(parentIndex) {
          parentClicked = parentClicked || (self.nodes[parentIndex] === undefined || self.nodes[parentIndex].prop('clicked'));
        });
      } else {
        parentClicked = true;
      }
      if (parentClicked) {
        //this means that the previous step was completed, so this click can continue
        node.attr({
          rect: {
            fill: 'black'
          },
          text: {
            fill: 'white'
          }
        });
        node.prop('clicked', true);
        //this is only really needed for testing purposes...
        V(this).addClass('clicked');
        //now send the data back to the server to store it in the session so a user can resume (using the same browser)
        $.post('/record-step', {step: index, title: title}, function(data, status, jqxhr) {
          console.log(status);
        });
      } else {
        //do something here?
        window.alert('please complete the previous step before this one');
      }
    });
  }
};
